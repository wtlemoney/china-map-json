const Alladcode = require('./all');
const ChinaData = require('./china');
const ProvinceData = require('./province');
const FiveCityData = require('./city');
const ProvinceMap = require('./pair');

module.exports = {
  Alladcode, ChinaData, ProvinceData, FiveCityData, ProvinceMap
};
