const anhui = require('./anhui');
const aomen = require('./aomen');
const beijing = require('./beijing');
const chongqing = require('./chongqing');
const fujian = require('./fujian');
const gansu = require('./gansu');
const guangdong = require('./guangdong');
const guangxi = require('./guangxi');
const guizhou = require('./guizhou');
const hainan = require('./hainan');
const hebei = require('./hebei');
const henan = require('./henan');
const heilongjiang = require('./heilongjiang');
const hubei = require('./hubei');
const hunan = require('./hunan');
const jilin = require('./jilin');
const jiangsu = require('./jiangsu');
const jiangxi = require('./jiangxi');
const liaoning = require('./liaoning');
const neimenggu = require('./neimenggu');
const ningxia = require('./ningxia');
const qinghai = require('./qinghai');
const shandong = require('./shandong');
const shanxi = require('./shanxi');
const shanxi1 = require('./shanxi1');
const shanghai = require('./shanghai');
const sichuan = require('./sichuan');
const taiwan = require('./taiwan');
const tianjin = require('./tianjin');
const xizang = require('./xizang');
const xianggang = require('./xianggang');
const xinjiang = require('./xinjiang');
const yunnan = require('./yunnan');
const zhejiang = require('./zhejiang');

module.exports = {
  anhui, aomen, beijing, chongqing, fujian, guizhou, guangxi, guangdong,
  gansu, hunan, hubei, heilongjiang, hebei, henan, hainan, jiangxi, jiangsu, jilin,
  liaoning, ningxia, neimenggu, qinghai, sichuan, shanghai, shandong, shanxi,
  shanxi1, tianjin, taiwan, xinjiang, xianggang, xizang, yunnan, zhejiang
};
