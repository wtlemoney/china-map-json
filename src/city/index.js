const dalian = require('./dalian');
const ningbo = require('./ningbo');
const qingdao = require('./qingdao');
const shenzhen = require('./shenzhen');
const xiamen = require('./xiamen');

module.exports = {
  dalian, ningbo, qingdao, shenzhen, xiamen
};