module.exports = [
  { en: 'hebei', zh: '河北省' },
  { en: 'shanxi', zh: '山西省' },
  { en: 'shanxi1', zh: '陕西省' },
  { en: 'neimenggu', zh: '内蒙古自治区' },
  { en: 'jilin', zh: '吉林省' },
  { en: 'heilongjiang', zh: '黑龙江省' },
  { en: 'jiangsu', zh: '江苏省' },
  { en: 'zhejiang', zh: '浙江省' },
  { en: 'anhui', zh: '安徽省' },
  { en: 'fujian', zh: '福建省' },
  { en: 'jiangxi', zh: '江西省' },
  { en: 'shandong', zh: '山东省' },
  { en: 'henan', zh: '河南省' },
  { en: 'hubei', zh: '湖北省' },
  { en: 'hunan', zh: '湖南省' },
  { en: 'guangdong', zh: '广东省' },
  { en: 'guangxi', zh: '广西壮族自治区' },
  { en: 'hainan', zh: '海南省' },
  { en: 'sichuan', zh: '四川省' },
  { en: 'guizhou', zh: '贵州省' },
  { en: 'yunnan', zh: '云南省' },
  { en: 'xizang', zh: '西藏自治区' },
  { en: 'gansu', zh: '甘肃省' },
  { en: 'qinghai', zh: '青海省' },
  { en: 'ningxia', zh: '宁夏回族自治区' },
  { en: 'xinjiang', zh: '新疆维吾尔自治区' },
  { en: 'hainan', zh: '海南省' },
  { en: 'sichuan', zh: '四川省' },
  { en: 'guizhou', zh: '贵州省' },
  { en: 'yunnan', zh: '云南省' },
  { en: 'beijing', zh: '北京市' },
  { en: 'tianjin', zh: '天津市' },
  { en: 'chongqing', zh: '重庆市市' },
  { en: 'aomen', zh: '澳门特别行政区' },
  { en: 'xianggang', zh: '香港特别行政区' },
  { en: 'aomen', zh: '澳门特别行政区' },
  { en: 'ningbo', zh: '宁波市' },
  { en: 'dalian', zh: '大连市' },
  { en: 'qingdao', zh: '青岛市' },
  { en: 'shenzhen', zh: '深圳市' },
  { en: 'xiamen', zh: '厦门市' }
]

